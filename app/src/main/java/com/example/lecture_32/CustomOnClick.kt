package com.example.lecture_32

interface CustomOnClick {
    fun onClickAdd(adapterPosition: Int)
    fun onClickRemove(adapterPosition: Int)
}