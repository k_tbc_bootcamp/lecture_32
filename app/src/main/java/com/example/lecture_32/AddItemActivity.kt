package com.example.lecture_32

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_add_item.*

class AddItemActivity : AppCompatActivity() {

    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-items"
        ).build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)

        init()
    }

    private fun init() {
        if (intent.hasExtra("title")) {
            titleEditText.setText(intent.getStringExtra("title"))
            pictureEditText.setText(intent.getStringExtra("picture"))
            descriptionEditText.setText(intent.getStringExtra("description"))
            addItemButton.visibility = View.GONE
            changeItemButton.visibility = View.VISIBLE
        }

        changeItemButton.setOnClickListener {
            val title = titleEditText.text.toString()
            val picture = pictureEditText.text.toString()
            val description = descriptionEditText.text.toString()

            if ((title.length in 5..30) && (description.length in 32..300) && picture.isNotEmpty()) {
                val itemId = intent.getStringExtra("itemId")!!
                AsyncTask.execute {
                    db.itemDao().update(title, picture, description, itemId)
                    val intent = Intent(this, MainActivity::class.java).apply {
                        putExtra("itemId", itemId)
                    }
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
            else {
                Toast.makeText(this, "Please appropriately fill in the fields", Toast.LENGTH_LONG).show()
            }
        }

        addItemButton.setOnClickListener {
            val title = titleEditText.text.toString()
            val picture = pictureEditText.text.toString()
            val description = descriptionEditText.text.toString()

            if ((title.length in 5..30) && (description.length in 32..300) && picture.isNotEmpty()) {
                val item = Item(title, picture, description)
                AsyncTask.execute {
                    db.itemDao().insert(item)
                    d("isInserted", " ${item.title}")
                    val intent = Intent(this, MainActivity::class.java)
                    setResult(Activity.RESULT_OK)
                    finish()
                }

            }
            else {
                Toast.makeText(this, "Please appropriately fill in the fields", Toast.LENGTH_LONG).show()
            }
        }
    }
}
