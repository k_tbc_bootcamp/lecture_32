package com.example.lecture_32

import androidx.room.*

@Dao
interface ItemDao {
    @Query("SELECT * FROM items")
    fun getAll(): List<Item>

    @Query("SELECT * FROM items WHERE itemId = :itemId")
    fun loadById(itemId: String): Item

    @Query("SELECT * FROM items WHERE itemId = :itemId")
    fun findById(itemId: String): Item

    @Query("SELECT * FROM items ORDER BY itemId DESC LIMIT 1")
    fun getLast(): Item

    @Insert
    fun insert(item: Item)

    @Query("UPDATE items SET title = :title, picture = :picture, description = :description  WHERE itemId = :itemId")
    fun update(title: String, picture: String, description: String, itemId: String)

    @Delete
    fun delete(item: Item)
}
