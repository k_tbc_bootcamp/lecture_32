package com.example.lecture_32

import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.recyclerview_item_layout.*

class MainActivity : AppCompatActivity() {

    private var position = 0

    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-items"
        ).build()
    }

    companion object {
        const val REQUEST_CODE_ADD = 1
        const val REQUEST_CODE_CHANGE = 2
    }

    private val items = mutableListOf<Item>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {

        checkForEmpty()

        //add item listener
        addItemButton.setOnClickListener {
            val intent = Intent(this, AddItemActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE_ADD)
        }

        //adapter
        adapter = RecyclerViewAdapter(items, object: CustomOnClick {
            override fun onClickAdd(adapterPosition: Int) {
                position = adapterPosition
                val item = items[position]
                val itemId = item.itemId

                val intent = Intent(this@MainActivity, AddItemActivity::class.java).apply {
                    putExtra("title", item.title)
                    putExtra("picture", item.picture)
                    putExtra("description", item.description)
                    putExtra("itemId", "$itemId")
                }

                startActivityForResult(intent, REQUEST_CODE_CHANGE)
            }

            override fun onClickRemove(adapterPosition: Int) {
                AsyncTask.execute {
                    val item = items[adapterPosition]
                    db.itemDao().delete(item)
                    runOnUiThread {
                        items.removeAt(adapterPosition)
                        adapter.notifyItemRemoved(adapterPosition)
                    }
                }
                checkForEmpty()
            }

        })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        recyclerView.scrollToPosition(0)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        checkForEmpty()

        //add
        if (requestCode == REQUEST_CODE_ADD && resultCode == Activity.RESULT_OK) {
            AsyncTask.execute {
                val item = db.itemDao().getLast()
                runOnUiThread {
                    items.add(0, item)
                    adapter.notifyItemInserted(0)
                }
            }

        }

        //change
        if (requestCode == REQUEST_CODE_CHANGE && resultCode == Activity.RESULT_OK) {
            val itemId = data!!.getStringExtra("itemId")
            AsyncTask.execute {
                val item = db.itemDao().loadById(itemId!!)
                runOnUiThread {
                    d("title", "${item.title}")
                    items[position] = item
                    adapter.notifyItemChanged(position)
                }
            }
        }
    }

    private fun checkForEmpty() {
        AsyncTask.execute {
            if (db.itemDao().getAll().isNotEmpty()) {
                runOnUiThread {
                    recyclerView.visibility = View.VISIBLE
                    itemsIsEmptyTextView.visibility = View.GONE
                }
            }
            else {
                runOnUiThread {
                    recyclerView.visibility = View.GONE
                    itemsIsEmptyTextView.visibility = View.VISIBLE
                }
            }
        }
    }
}
