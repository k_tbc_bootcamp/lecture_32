package com.example.lecture_32

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "items")
data class Item(
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "picture") val picture: String?,
    @ColumnInfo(name = "description") val description: String?
) {
    @PrimaryKey(autoGenerate = true) var itemId: Int = 0
}
