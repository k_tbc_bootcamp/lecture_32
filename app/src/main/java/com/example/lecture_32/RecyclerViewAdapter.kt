package com.example.lecture_32

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recyclerview_item_layout.view.*

class RecyclerViewAdapter(
    private val items: MutableList<Item>,
    private val customOnClick: CustomOnClick
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recyclerview_item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var item: Item

        fun onBind() {
            item = items[adapterPosition]
            itemView.setOnClickListener {
                customOnClick.onClickAdd(adapterPosition)
            }
            itemView.removeItemButton.setOnClickListener {
                customOnClick.onClickRemove(adapterPosition)
            }
            itemView.titleTextView.text = item.title
            Glide.with(itemView)
                .load(item.picture)
                .placeholder(R.mipmap.ic_launcher)
                .into(itemView.pictureImageView)
            itemView.descriptionTextView.text = item.description
        }
    }
}